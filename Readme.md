﻿BOJ-AutoCommit
=========
### Node.js 

`BOJ-AutoCommit`은 Baekjoon Online Judge(www.acmicpc.net)의 다양한 알고리즘 문제를 풀어서 제출하고 정답을 맞추면 원격저장소에 Source Code를 Push하는 툴입니다. 일정한 시간을 주기로 Baekjoon Online Judge의 회원 ID에 해당하는 정답을 맞춘 문제들을 검색하고, Repo에 해당 문제가 존재하지 않으면 Source Code를 다운로드 받아 Repo에 저장하고, Git을 사용하여 Add, Commit, Push하여 줍니다.

Installation
-----------
First
``` bash
$ git clone https://github.com/ISKU/BOJ-AutoCommit
```

dependency
```
$ npm install -g phantomjs
$ npm install -g casperjs
```

node.js와 git-bash는 기본적으로 설치되어 있어야 합니다.
phantomjs 는 python을 필요로 합니다.
casperjs는 phantomjs를 필요로 합니다.

How to use
----------
info.json에 BOJ의 회원 정보와, 원격저장소의 회원 정보 및 원격저장소 Url을 입력하여야 합니다.
```
{
	"boj_id": "example_my_boj_id",
	"boj_password": "example_my_boj_password",
	"git_id": "example_my_github_id",
	"git_password": "examp_my_github_password",
	"remoteUrl": "https://github.com/ISKU/Algorithm"
}
```

반드시 info.json에 올바른 정보를 입력하고 다음과 같이 툴을 실행합니다.
```
$ node app.js info.json    or    $ ./app.js info.json
```

tool이 쉬고 있는 시간이 길기 때문에 다음과 같이 백그라운드에서 실행을 권장합니다.
```
$ nohup node app.js info.json &
```

Default
--------
Source Code 저장은 문제번호로 directory를 생성 한 후, 하위 directory에 문제번호를 제목으로 파일이 저장됩니다.
Commit Message는 기본적으로 https://www.acmicpc.net/problem/문제번호 입니다.
약 10분마다 맞춘 문제를 검색하고 새롭게 맞춘 문제가 있으면 원격저장소에 Push합니다.

Extension
---------
tool을 확장하여 자유롭게 자신의 원격저장소를 관리할 수 있습니다.
info.json에 다음과 같이 원하는 option을 설정하세요.

```
{
	"boj_id": "example_my_boj_id",
	"boj_password": "example_my_boj_password",
	"git_id": "example_my_github_id",
	"git_password": "examp_my_github_password",
	"remoteUrl": "https://github.com/ISKU/Algorithm",
	
	"commitMessage": "https://www.acmicpc.net/problem/<NO>",
	"sourceTree": "Algorithm/<NO>/",
	"mkdir": true,
	"secret": true,
	"poll": 60000,
	"sourceName": "Main";
}
```

Key Options

`BOJ-AutoCommit` accepts the following options:

| **Key**            | **Description**
|:-------------------|:-------------------------------------------------
| `commitMessage`    | 원격저장소에 표시될 Commit Message를 설정하세요.
| `sourceTree`       | 원하는 위치에 Source Code를 저장합니다. 시작 directory는 Repo 이름과 일치하여야 합니다.
| `mkdir`            | directory를 생성하여 Source Code를 저장할 수 있습니다. 
| `poll`             | BOJ의 맞은 문제를 검색하는 주기를 설정할 수 있습니다. ex: 600000
| `secret`           | BOJ에서 Source Code를 비공개로 설정하면 해당 문제는 다운로드하지 않습니다.
| `sourceName`       | Source Code 파일 이름을 지정할 수 있습니다. <NO>를 사용하여 문제번호로 저장하는 것을 추천합니다.


Example
--------
https://github.com/ISKU/Algorithm
위 repo는 BOJ-AutoCommit을 사용하여 Source Code를 관리하고 있습니다.


License
--------
MIT

Author
----------
Kim Min-Ho (ISKU) <minho1a@hanmail.net>